window.addEventListener("load", () => {
  const form = document.querySelector("#form");
  form.addEventListener("submit", (e) => {
    const validacion = validaCampos();
    //if (!validacion) {
    e.preventDefault();
    //}
    if (validacion) {
      const payload = new FormData(form);
      fetch(
        "https://readinessitsistemasdeintegracioncl--salescrm1.my.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",
        {
          method: "POST",
          mode: "no-cors",
          body: payload,
        }
      ).then((res) => {
        console.log(res);
        window.alert("Formulario enviado");
        resetValues();
      });
    }
  });
});

const validaCampos = () => {
  if (!validateEmail(email) && !validateCompany(company)) {
    return false;
  }
  return true;
};
const validateEmail = (email) => {
  const emailValue = email.value.trim();
  if (!regExEmailValid(emailValue)) {
    errorMessage(email, "* Ingrese un email válido ej. example@mail.com");
    console.log("no valido email");
    return false;
  }
  return true;
};
const validateCompany = (company) => {
  const companyValue = company.value.trim();
  if (companyValue === "") {
    errorMessage(company, "* Ingrese una empresa");
    console.log("no valido company");
    return false;
  }
  return true;
};
const errorMessage = (input, msje) => {
  const formItems = input.parentElement;
  const aviso = formItems.querySelector("p");
  aviso.innerText = msje;
  formItems.className = "form__items error";
};
const regExEmailValid = (email) => {
  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    email
  );
};
const regexPhone = (phone) => {
  return /^[0-9+]+$/.test(phone);
};
const checkPhone = (input) => {
  const phoneValue = input.value.trim();
  if (!regexPhone(phoneValue)) {
    console.log("no valido phone");
    input.value = input.value.replace(/((\D)|(^\+))$/g, "");
    return false;
  }
};
const checkOnlyNumber = (input) => {
  input.value = input.value.replace(/\D+/g, "");
};
const consolidarInformacion = (cloud, users, customers, description) => {
  const cloudsInput = cloud;
  const descriptionInput = description;
  const usersInput = users;
  const customersInput = customers;
  const cloudsValues = [];
  cloudsInput.forEach((e) => {
    if (e.checked) {
      cloudsValues.push(e.value);
    }
  });
  console.log(cloudsInput);
  descriptionInput.value =
    "Clouds Salesforce: " +
    cloudsValues +
    " | " +
    "N de usuarios: " +
    usersInput.value +
    " | " +
    "N de clientes: " +
    customersInput.value;
};
const copySelected = (input) => {
  const selected = [];
  input.forEach((element) => {
    if (element.checked) {
      selected.push(element.value);
    }
  });
  console.log(selected);
  return selected;
};
const resetValues = () => {
  const inputs = document.querySelectorAll("input");
  inputs.forEach((element) => {
    element.value = "";
  });
  const checkbox = document.querySelectorAll("input[type=checkbox]");
  checkbox.forEach((element) => {
    element.checked = false;
  });
};
